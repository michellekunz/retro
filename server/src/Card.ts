import {User} from './User';

export interface Card {
  id: string;
  comment?: string;
  gifs: string[];
  authors: User[];
  type: Type;
  votes: number;
}

export enum Type {
  POSITIVE, NEGATIVE
}
