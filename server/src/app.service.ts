import { Injectable } from '@nestjs/common';
import { Card, Type } from './Card';
import { User } from './User';
import { v4 as uuidv4 } from 'uuid';
import { Subject } from 'rxjs';
import { UserVotes } from './UserVotes';

export interface ResetEvent {
  eventType: 'reset';
}

export interface TimerUpdate {
  eventType: 'timerUpdate';
  date: Date | null;
}

export interface CommentMode {
  eventType: 'commentMode';
  enabled: boolean;
}

export interface AddCardEvent {
  eventType: 'add';
  id: string;
  comment?: string;
  gifs: string[];
  authors: User[];
  type: Type;
  votes: number;
}

export interface UpdateCardEvent {
  eventType: 'update';
  id: string;
  comment?: string;
  gifs: string[];
  authors: User[];
  type: Type;
  votes: number;
}

export interface DeleteCardEvent {
  eventType: 'delete';
  id: string;
}

export interface UpVoteEvent {
  eventType: 'upVote';
  cardId: string;
  userId: string;
}

export interface DownVoteEvent {
  eventType: 'downVote';
  cardId: string;
  userId: string;
}

export interface RearrangeCardsEvent {
  eventType: 'rearrange';
  ids: string[];
}

export type CardEvent =
  | ResetEvent
  | DeleteCardEvent
  | UpdateCardEvent
  | AddCardEvent
  | TimerUpdate
  | CommentMode
  | UpVoteEvent
  | DownVoteEvent
  | RearrangeCardsEvent

@Injectable()
export class AppService {
  private readonly maxVotes = 3;
  private hasScrumMaster: boolean = false;
  private users = new Map<string, User>();
  private cards = new Map<string, Card>();
  private commentMode = true;
  private timerDate: Date | null = null;
  cardEvents = new Subject<CardEvent>();

  createUser(userName: string, admin: boolean): User | null {
    if (![...this.users.values()].find((user) => user.userName === userName)) {
      const user: User = {
        id: uuidv4(),
        userName,
        isAdmin: false,
        votes: new Map<string, number>(),
      };
      let hasWorked = true;

      if (admin) {
        if (!this.hasScrumMaster && user) {
          user.isAdmin = true;
          this.hasScrumMaster = true;
        } else {
          hasWorked = false;
        }
      }

      if (hasWorked) {
        this.users.set(user.id, user);
      }
      return user;
    }
    return null;
  }

  deleteUser(userId: string) {
    if (this.users.get(userId)?.isAdmin) {
      this.hasScrumMaster = false;
    }
    this.users.delete(userId);
  }

  existsUser(user: User): boolean {
    return user && null != this.users.get(user.id);
  }

  addCard(card: Card) {
    card.id = uuidv4();
    this.cards.set(card.id, card);
    this.cardEvents.next({
      authors: card.authors,
      comment: card.comment,
      gifs: card.gifs,
      eventType: 'add',
      id: card.id,
      type: card.type,
      votes: card.votes,
    });
  }

  upVoteCard(card: Card, userId: string) {
    const user = this.users.get(userId);
    if (user && this.allUserVotes(user.votes) < this.maxVotes) {
      const voteCount = user.votes.get(card.id) ?? 0;
      user.votes.set(card.id, (voteCount > 0 ? voteCount : 0) + 1);
      this.cardEvents.next({
        eventType: 'upVote',
        cardId: card.id,
        userId,
      });
      card.votes++;
      this.cards.set(card.id, card);
    }
  }

  downVoteCard(card: Card, userId: string) {
    const user = this.users.get(userId);
    if (user && user.votes.has(card.id)) {
      const voteCount = user.votes.get(card.id) ?? 0;
      user.votes.set(card.id, voteCount - 1);
      if (voteCount - 1 === 0) {
        user.votes.delete(card.id);
      }
      this.cardEvents.next({
        eventType: 'downVote',
        cardId: card.id,
        userId,
      });
      card.votes--;
      this.cards.set(card.id, card);
    }
  }

  saveCard(card: Card) {
    if (this.isUserAuthorized(card)) {
      this.cards.set(card.id, card);
      this.cardEvents.next({
        authors: card.authors,
        comment: card.comment,
        gifs: card.gifs,
        eventType: 'update',
        id: card.id,
        type: card.type,
        votes: card.votes,
      });
    }
  }

  rearrangeCard(cards: Card[], userId: string) {
    if (this.users.get(userId)?.isAdmin) {
      this.cardEvents.next({
        eventType: 'rearrange',
        ids: cards.map((card) => card.id),
      });
    }
  }

  mergeCards(sourceCard: Card, targetCard: Card, userId: string) {
    if (this.users.get(userId)?.isAdmin) {
      const uniqueAuthors = [...targetCard.authors, ...sourceCard.authors]
        .filter((user, index, authors) => authors.findIndex(other => other.id === user.id) === index);
      const mergedComments = AppService.getMergedComments(targetCard, sourceCard);
      const mergedGifs = [...targetCard.gifs, ...sourceCard.gifs];
      const mergedCard = {
        id: '',
        comment: mergedComments,
        gifs: mergedGifs,
        authors: uniqueAuthors,
        type: targetCard.type,
        votes: targetCard.votes + sourceCard.votes,
      };
      this.addCard(mergedCard);
      this.deleteCard(sourceCard, false);
      this.deleteCard(targetCard, false);
    }
  }

  private static getMergedComments(targetCard: Card, sourceCard: Card) {
    if (!targetCard.comment && !sourceCard.comment) {
      return '';
    } else if (!targetCard.comment) {
      return sourceCard.comment;
    } else if (!sourceCard.comment) {
      return targetCard.comment;
    }
    return targetCard.comment + '\n------\n' + sourceCard.comment;
  }

  deleteCard(card: Card, withCheck: boolean): boolean {
    if (!withCheck || this.isUserAuthorized(card)) {
      const deleted = this.cards.delete(card.id);
      this.cardEvents.next({
        eventType: 'delete',
        id: card.id,
      });
      return deleted;
    }
    return false;
  }

  getCards() {
    const result: Card[] = [];
    for (const card of this.cards.values()) {
      result.push(card);
    }
    return result;
  }

  getVotes(): UserVotes[] {
    const votes: UserVotes[] = [];
    for (const user of this.users.values()) {
      votes.push({
        userName: user.userName,
        votes: this.allUserVotes(user.votes),
      });
    }
    return votes;
  }

  getRemainingUserVotes(userId: string): number {
    return this.maxVotes - this.allUserVotes(this.users.get(userId)?.votes);
  }

  setTime(minutes: number) {
    this.timerDate = new Date(new Date().getTime() + minutes * 60 * 1000);
    this.cardEvents.next({ eventType: 'timerUpdate', date: this.timerDate });
  }

  getTime(): Date | null {
    return this.timerDate;
  }

  setCommentMode(mode: boolean) {
    this.commentMode = mode;
    this.cardEvents.next({ eventType: 'commentMode', enabled: mode });
  }

  getCommentMode(): boolean {
    return this.commentMode;
  }

  resetAll() {
    this.users.clear();
    this.cards.clear();
    this.hasScrumMaster = false;
    this.commentMode = true;
    this.timerDate = null;
    this.cardEvents.next({ eventType: 'reset' });
  }

  private isUserAuthorized(card: Card) {
    const original = this.cards.get(card?.id);
    return original && card && original.authors?.map(user => user.id).some(originalUserId => card.authors?.map(user => user.id).includes(originalUserId));
  }

  private allUserVotes(votes: Map<string, number> | undefined): number {
    let totalVotes = 0;
    if (votes) {
      votes.forEach((value) => (totalVotes += value));
    }
    return totalVotes;
  }

  clear(password: string): boolean {
    if (password === 'alleslöschen?') {
      this.resetAll();
      return true;
    }
    return false;
  }
}
