export interface UserVotes {
  userName: string;
  votes: number;
}
