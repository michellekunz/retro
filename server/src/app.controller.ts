import { Body, Controller, Get, Post, Sse } from '@nestjs/common';
import { AppService, CardEvent } from './app.service';
import { User } from './User';
import { Card } from './Card';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {
  }

  @Post('createUser')
  createUser(@Body() body: { userName: string, admin: boolean }): User | null {
    return this.appService.createUser(body.userName, body.admin);
  }

  @Post('deleteUser')
  deleteUser(@Body() body: { userId: string }): void {
    return this.appService.deleteUser(body.userId);
  }

  @Post('existsUser')
  existsUser(@Body() user: User): boolean {
    return this.appService.existsUser(user);
  }

  @Post('addCard')
  addCard(@Body() card: Card): void {
    this.appService.addCard(card);
  }

  @Post('saveCard')
  saveCard(@Body() card: Card): void {
    this.appService.saveCard(card);
  }

  @Post('upVote')
  upVote(@Body() body: { card: Card, userId: string }): void {
    this.appService.upVoteCard(body.card, body.userId);
  }

  @Post('downVote')
  downVoteCard(@Body() body: { card: Card, userId: string }): void {
    this.appService.downVoteCard(body.card, body.userId);
  }

  @Post('rearrange')
  rearrangeCard(@Body() body: { cards: Card[], userId: string }): void {
    this.appService.rearrangeCard(body.cards, body.userId);
  }

  @Post('merge')
  mergeCards(@Body() body: { sourceCard: Card, targetCard: Card, userId: string }): void {
    this.appService.mergeCards(body.sourceCard, body.targetCard, body.userId);
  }

  @Post('deleteCard')
  deleteCard(@Body() card: Card): boolean {
    return this.appService.deleteCard(card, true);
  }

  @Get('resetAll')
  resetAll() {
    this.appService.resetAll();
  }

  @Get('cards')
  getCards() {
    return this.appService.getCards();
  }

  @Post('setTime')
  setTime(@Body() body: { minutes: number }) {
    this.appService.setTime(body.minutes);
  }

  @Get('time')
  getTime() {
    return this.appService.getTime();
  }

  @Post('setCommentMode')
  setCommentMode(@Body() body: { mode: boolean }) {
    this.appService.setCommentMode(body.mode);
  }

  @Get('commentMode')
  getCommentMode() {
    return this.appService.getCommentMode();
  }

  @Post('votes')
  getVotes(@Body() body: { userId: string }) {
    return this.appService.getRemainingUserVotes(body.userId);
  }

  @Post('allVotes')
  getAllVotes() {
    return this.appService.getVotes();
  }

  @Post('clear')
  clear(@Body() body: { password: string }): boolean {
    return this.appService.clear(body.password);
  }

  @Sse('events')
  events(): Observable<{ data: CardEvent }> {
    return this.appService.cardEvents.asObservable().pipe(map(evt => ({ data: evt })));
  }
}


