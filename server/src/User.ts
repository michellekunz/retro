export interface User {
  id: string;
  userName: string;
  isAdmin: boolean;
  votes: Map<string, number>;
}
