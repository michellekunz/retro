FROM node:18-alpine as builder

WORKDIR /build

COPY package.json .
COPY package-lock.json .
RUN npm ci

COPY browserslist .
COPY tsconfig.json .
COPY tsconfig.app.json .
COPY tsconfig.server.json .
COPY tsconfig.build.json .
COPY angular.json .
COPY tailwind.config.js .

COPY src src
COPY server server

RUN node_modules/.bin/ng build -c production
RUN node_modules/.bin/nest build

#######################################################################
FROM node:18-alpine as runtime

ENV NODE_ENV production
ENV PORT=3000
LABEL fly_launch_runtime="nodejs"

COPY package.json .
COPY package-lock.json .
RUN npm install --only=production && npm cache clean --force

COPY --from=builder /build/dist/Retro static
COPY --from=builder /build/dist/server/src .

CMD [ "node", "main.js" ]
