import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { UserService } from '../services/user.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(public auth: UserService, private router: Router) {}

  async canActivate() {
    if (!(await this.auth.isLoggedIn())) {
      return this.router.createUrlTree(['/login']);
    }
    return true;
  }
}
