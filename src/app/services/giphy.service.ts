import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { firstValueFrom } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class GiphyService {

  constructor(private http: HttpClient) {
  }

  private readonly size = 16;

  async searchGiphy(searchQuery: string) {
    return await firstValueFrom(this.http.get<Giphy>(this.getUrl(searchQuery, this.initialPagination())));
  }

  async nextGiphy(searchQuery: string, pagination?: Pagination) {
    if (!pagination) {
      pagination = this.initialPagination();
    }
    pagination.offset += this.size;
    return await firstValueFrom(this.http.get<Giphy>(this.getUrl(searchQuery, pagination)));
  }

  async previousGiphy(searchQuery: string, pagination?: Pagination) {
    if (!pagination) {
      pagination = this.initialPagination();
    }
    pagination.offset -= this.size;
    return await firstValueFrom(this.http.get<Giphy>(this.getUrl(searchQuery, pagination)));
  }

  initialPagination() {
    return {
      offset: 0,
    };
  }

  private getUrl(searchQuery: string, pagination: Pagination) {
    return `https://api.giphy.com/v1/gifs/search?api_key=${environment.giphyApiKey}&q=${searchQuery}&limit=${this.size}&offset=${pagination.offset}`;
  }
}

export interface Giphy {
  data: any[],
  pagination: Pagination,
  meta: any,
}

export interface Pagination {
  offset: number,
  total_count?: number,
  count?: number
}
