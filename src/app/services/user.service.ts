import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { User } from '../../../server/src/User';
import { UserVotes } from '../../../server/src/UserVotes';
import { firstValueFrom } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  user = UserService.loadUser();

  private static loadUser(): User | null {
    const data = localStorage.getItem('user');
    if (data) {
      return JSON.parse(data);
    }
    return null;
  }

  private saveUser(user: User | null) {
    localStorage.setItem('user', JSON.stringify(user));
    this.user = user;
  }

  constructor(private http: HttpClient) {}

  async join(userName: string, admin: boolean): Promise<string | null> {
    if (!userName) {
      return 'Es wurde kein Benutzername übergeben';
    }
    this.saveUser(
      (await firstValueFrom(
        this.http.post<User>(`${environment.apiUrl}/createUser`, { userName, admin }),
      )) ?? null,
    );
    if (this.user == null) {
      return 'Dieser Benutzername ist bereits vergeben.';
    }
    if (admin && !this.user.isAdmin) {
      return 'Es wurde bereits ein Administrator gesetzt';
    }
    return null;
  }

  async leave() {
    if (this.user) {
      await firstValueFrom(
        this.http.post(`${environment.apiUrl}/deleteUser`, { userId: this.user.id }),
      );
      localStorage.removeItem('user');
    }
  }

  async isLoggedIn(): Promise<boolean> {
    if (!this.user) {
      return false;
    }
    return (
      (await firstValueFrom(
        this.http.post<boolean>(`${environment.apiUrl}/existsUser`, this.user),
      )) ?? false
    );
  }

  async resetAll() {
    await firstValueFrom(
      this.http.get(`${environment.apiUrl}/resetAll`),
    );
    this.saveUser(null);
  }

  async getVotes() {
    if (!this.user) {
      return 0;
    }
    return firstValueFrom(await
      this.http.post<number>(`${environment.apiUrl}/votes`, { userId: this.user.id }),
    );
  }

  async getAllVotes() {
    return await firstValueFrom(
      this.http.post<UserVotes[]>(`${environment.apiUrl}/allVotes`, null),
    );
  }
}
