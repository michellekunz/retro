import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Card } from '../../../server/src/Card';
import { firstValueFrom } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CardService {
  constructor(private http: HttpClient) {}

  async getCards() {
    return await firstValueFrom(
      this.http.get<Card[]>(`${environment.apiUrl}/cards`),
    );
  }

  async addCard(card: Card) {
    await firstValueFrom(
      this.http.post(`${environment.apiUrl}/addCard`, card),
    );
  }

  async deleteCard(card: Card): Promise<boolean> {
    return (await firstValueFrom(
      this.http.post<boolean>(`${environment.apiUrl}/deleteCard`, card),
    )) ?? false;
  }

  async setCommitMode(mode: boolean) {
    await firstValueFrom(
      this.http.post(environment.apiUrl + '/setCommentMode', { mode }),
    );
  }

  async saveCard(card: Card) {
    await firstValueFrom(
      this.http.post(`${environment.apiUrl}/saveCard`, card),
    );
  }

  async upVoteCard(card: Card, userId: string) {
    await firstValueFrom(
      this.http.post(`${environment.apiUrl}/upVote`, { card, userId }),
    );
  }

  async downVoteCard(card: Card, userId: string) {
    await firstValueFrom(
      this.http.post(`${environment.apiUrl}/downVote`, { card, userId }),
    );
  }

  async rearrangeCards(cards: Card[], userId: string) {
    await firstValueFrom(
      this.http.post(`${environment.apiUrl}/rearrange`, { cards, userId }),
    );
  }

  async mergeCards(sourceCard: Card, targetCard: Card, userId: string) {
    await firstValueFrom(
      this.http.post(`${environment.apiUrl}/merge`, { sourceCard, targetCard, userId }),
    );
  }

  async getTime() {
    return await firstValueFrom(
      this.http.get<Date>(`${environment.apiUrl}/time`),
    );
  }

  async getCommentMode() {
    return await firstValueFrom(
      this.http.get<boolean>(`${environment.apiUrl}/commentMode`),
    );
  }
}
