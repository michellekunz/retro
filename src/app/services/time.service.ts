import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class TimeService {
  constructor(private http: HttpClient) {}

  async setTime(minutes: number) {
    await this.http
      .post(environment.apiUrl + '/setTime', { minutes })
      .toPromise();
  }
}
