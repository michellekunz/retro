import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';

export interface Role {
  id: string;
  name: string;
  description: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  userName = '';
  roles: Role[] = [
    {
      id: 'scrum-master',
      name: 'Scrum Master',
      description: 'Ich leite das Retro',
    },
    {
      id: 'dev-team',
      name: 'Entwickler',
      description: 'Ich nehme am Retro teil',
    },
  ];
  selectedRole = 'dev-team';
  error: string | null = null;

  constructor(private router: Router, private userService: UserService) {
  }

  async ngOnInit() {
    this.error = null;
    if (await this.userService.isLoggedIn()) {
      await this.toDashboard();
    }
  }

  async join() {
    this.error = await this.userService.join(
      this.userName,
      this.selectedRole === 'scrum-master',
    );
    await this.toDashboard();
  }

  async toDashboard() {
    if (!this.error) {
      await this.router.navigate(['/dashboard']);
    }
  }

  hasUserName() {
    return this.userName?.trim().length > 0;
  }
}
