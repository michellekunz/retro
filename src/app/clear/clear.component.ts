import { Component } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-clear',
  templateUrl: './clear.component.html',
  styleUrls: ['./clear.component.css'],
})
export class ClearComponent {
  password = '';
  successText: string | null = null;

  constructor(private http: HttpClient) {}

  async clear() {
    if (
      await this.http
        .post<boolean>(`${environment.apiUrl}/clear`, {
          password: this.password,
        })
        .toPromise()
    ) {
      this.successText = 'Erfolgreich';
    } else {
      this.successText = '';
    }
    this.password = '';
  }
}
