import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClearComponent } from './clear.component';
import { FormsModule } from '@angular/forms';
import { ClearRoutingModule } from './clear-routing.module';

@NgModule({
  declarations: [ClearComponent],
  imports: [CommonModule, FormsModule, ClearRoutingModule],
})
export class ClearModule {}
