import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VoteComponent } from './vote.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [VoteComponent],
  exports: [VoteComponent],
    imports: [CommonModule, FontAwesomeModule],
})
export class VoteModule {}
