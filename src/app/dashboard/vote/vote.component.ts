import { Component, Input } from '@angular/core';
import { UserVotes } from '../../../../server/src/UserVotes';
import { faChevronDown } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-vote',
  templateUrl: './vote.component.html',
  styleUrls: ['./vote.component.scss'],
})
export class VoteComponent {
  @Input()
  userVotes!: number;
  @Input()
  allVotes!: UserVotes[];
  @Input()
  showVotes!: boolean;
  @Input()
  isAdmin!: boolean;
  faDetails = faChevronDown;
  details = false;

  get totalVotes(): number {
    return this.allVotes.reduce((sum, current) => sum + current.votes, 0);
  }

  showDetails() {
    this.details = !this.details;
  }
}
