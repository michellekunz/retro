import { Component, Input, OnInit } from '@angular/core';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { Card, Type } from '../../../../server/src/Card';
import { UserService } from '../../services/user.service';
import { CardService } from '../../services/card.service';
import { User } from '../../../../server/src/User';
import { DragulaService } from 'ng2-dragula';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss'],
})
export class BoardComponent implements OnInit {
  positiveCard: Card | null = null;
  negativeCard: Card | null = null;
  user: User | null = null;
  faPlus = faPlus;
  cardsContainer = 'CARDS';
  cardContainer = 'CARD';
  @Input()
  positiveCards!: Card[];
  @Input()
  negativeCards!: Card[];
  @Input()
  commentMode!: boolean;

  constructor(
    private readonly userService: UserService,
    private readonly cardService: CardService,
    private readonly dragulaService: DragulaService,
  ) {
  }

  ngOnInit(): void {
    this.user = this.userService.user;
    this.initSortContainer();
    this.initMergeContainer();
  }

  private initMergeContainer() {
    if (!this.dragulaService.find(this.cardContainer)) {
      this.dragulaService.createGroup(this.cardContainer, {
        direction: 'vertical',
        copy: false,
        revertOnSpill: true,
        moves: (el, source, handle) => {
          return !!handle?.closest('.group-handle');
        },
      });
    }
    this.dragulaService.dropModel(this.cardContainer).subscribe(async (evt) => {
      let sourceCard = evt.sourceModel.at(0);
      let targetCard = evt.targetModel.at(0);
      if (this.user && sourceCard && targetCard) {
        await this.cardService.mergeCards(sourceCard, targetCard, this.user.id);
      } else {
        this.dragulaService.find(this.cardContainer).drake.cancel(true);
      }
    });
  }

  private initSortContainer() {
    if (!this.dragulaService.find(this.cardsContainer)) {
      this.dragulaService.createGroup(this.cardsContainer, {
        direction: 'vertical',
        copy: false,
        revertOnSpill: true,
        moves: (el, source, handle) => {
          return !!handle?.closest('.group-handle');
        },
      });
    }
    this.dragulaService.dropModel(this.cardsContainer).subscribe(async (evt) => {
      if (this.user) {
        await this.cardService.rearrangeCards(evt.sourceModel, this.user.id);
      }
    });
  }

  addPositiveCard() {
    if (!this.user) {
      return;
    }
    this.positiveCard = {
      id: '',
      comment: '',
      gifs: [],
      authors: [this.user],
      type: Type.POSITIVE,
      votes: 0,
    };
  }

  removePositiveCard() {
    this.positiveCard = null;
  }

  addNegativeCard() {
    if (!this.user) {
      return;
    }
    this.negativeCard = {
      id: '',
      comment: '',
      gifs: [],
      authors: [this.user],
      type: Type.NEGATIVE,
      votes: 0,
    };
  }

  removeNegativeCard() {
    this.negativeCard = null;
  }

  async deleteCard(card: Card) {
    await this.cardService.deleteCard(card);
  }
}
