import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Giphy, GiphyService } from '../../../../services/giphy.service';
import { faChevronLeft, faChevronRight, faXmark } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-gifs',
  templateUrl: './gifs.component.html',
  styleUrls: ['./gifs.component.scss'],
})
export class GifsComponent implements OnInit {
  giphy: Giphy | undefined;
  searchQuery: string = '';
  @Output()
  imageUrlChooseEvent = new EventEmitter<string>();
  @Output()
  closeEvent = new EventEmitter<void>();
  faXmark = faXmark;
  faChevronRight = faChevronRight;
  faChevronLeft = faChevronLeft;

  constructor(public readonly giphyService: GiphyService) {
  }

  async ngOnInit() {
  }

  async search() {
    this.giphy = await this.giphyService.searchGiphy(this.searchQuery);
  }

  async searchNext() {
    console.log(this.giphy);
    this.giphy = await this.giphyService.nextGiphy(this.searchQuery, this.giphy?.pagination);
  }

  async searchPrevious() {
    this.giphy = await this.giphyService.previousGiphy(this.searchQuery, this.giphy?.pagination);
  }

  hasNext() {
    if (!this.giphy?.pagination || !this.giphy.pagination.count || !this.giphy.pagination.total_count) {
      return false;
    }
    return (this.giphy.pagination.offset + this.giphy.pagination.count) < this.giphy.pagination.total_count;
  }

  hasPrevious() {
    if (!this.giphy?.pagination) {
      return false;
    }
    return this.giphy.pagination.offset > 0;
  }

  chooseImage(imageUrl: string) {
    this.imageUrlChooseEvent.emit(imageUrl);
    this.close();
  }

  close() {
    this.closeEvent.emit();
  }
}
