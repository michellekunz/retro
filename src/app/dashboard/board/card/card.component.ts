import { AfterViewInit, Component, ElementRef, EventEmitter, HostListener, Input, OnInit, Output, ViewChild } from '@angular/core';
import { faBars, faFaceSmile, faSave, faThumbsDown, faThumbsUp, faTrash, faXmark } from '@fortawesome/free-solid-svg-icons';
import { Card, Type } from '../../../../../server/src/Card';
import { CardService } from '../../../services/card.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent implements AfterViewInit, OnInit {
  @Input()
  userId!: string;
  @Input()
  card!: Card;
  @Input()
  backgroundColor!: string;
  @Input()
  editable = true;
  @Input()
  movable = false;
  @Output()
  delete: EventEmitter<void> = new EventEmitter<void>();
  @Output()
  save: EventEmitter<void> = new EventEmitter<void>();
  @ViewChild('textArea')
  textarea?: ElementRef;
  isAuthorized = false;
  showGifs = false;
  Type = Type;
  faSave = faSave;
  faTrash = faTrash;
  faBars = faBars;
  faThumbsUp = faThumbsUp;
  faThumbsDown = faThumbsDown;
  faFaceSmile = faFaceSmile;
  faXmark = faXmark;

  constructor(private readonly cardService: CardService) {
  }

  ngOnInit(): void {
    this.isAuthorized = this.card?.authors?.map(user => user.id).some(userId => userId === this.userId) || false;
  }

  ngAfterViewInit(): void {
    this.focusInput();
  }

  focusInput() {
    if (this.textarea && !this.card.id) {
      this.textarea.nativeElement.focus();
    }
  }

  async increment() {
    await this.cardService.upVoteCard(this.card, this.userId);
  }

  async decrement() {
    await this.cardService.downVoteCard(this.card, this.userId);
  }

  async saveCard() {
    if (this.card?.id) {
      await this.cardService.saveCard(this.card);
    } else {
      await this.cardService.addCard(this.card);
    }
    this.save.emit();
  }

  authors(card: Card) {
    return 'Author: ' + card.authors.map(user => user.userName).join(' & ');
  }

  openGifs() {
    this.showGifs = true;
  }

  @HostListener('window:keydown.esc')
  hideGifs() {
    this.showGifs = false;
  }

  addGif(gif: string, card: Card) {
    if (gif) {
      card.gifs = [...card.gifs, gif];
    }
  }

  deleteGif(gif: string, card: Card) {
    if (gif) {
      const gifs = [...card.gifs];
      gifs.splice(gifs.indexOf(gif), 1);
      card.gifs = gifs;
    }
  }
}
