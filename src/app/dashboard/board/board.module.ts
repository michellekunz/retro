import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BoardComponent } from './board.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CardComponent } from './card/card.component';
import { FormsModule } from '@angular/forms';
import { DragulaModule } from 'ng2-dragula';
import { AutosizeModule } from 'ngx-autosize';
import { GifsComponent } from './card/gifs/gifs.component';

@NgModule({
  declarations: [BoardComponent, CardComponent, GifsComponent],
  imports: [
    CommonModule,
    FontAwesomeModule,
    FormsModule,
    DragulaModule,
    AutosizeModule,
  ],
  exports: [BoardComponent, CardComponent, GifsComponent],
})
export class BoardModule {}
