import { Component, EventEmitter, Input, Output } from '@angular/core';
import { faClock, faComments, faThumbsDown, faThumbsUp, faTrash } from '@fortawesome/free-solid-svg-icons';
import { UserService } from '../../services/user.service';
import { TimeService } from '../../services/time.service';
import { Router } from '@angular/router';
import { CardService } from '../../services/card.service';

@Component({
  selector: 'app-administration',
  templateUrl: './administration.component.html',
  styleUrls: ['./administration.component.scss'],
})
export class AdministrationComponent {
  show = false;
  @Input()
  commentMode!: boolean;
  @Output()
  clearAll: EventEmitter<void> = new EventEmitter<void>();
  faThumbsUp = faThumbsUp;
  faThumbsDown = faThumbsDown;
  faClock = faClock;
  faTrash = faTrash;
  faComments = faComments;

  constructor(
    private readonly router: Router,
    private readonly userService: UserService,
    private readonly timeService: TimeService,
    private readonly cardService: CardService,
  ) {
  }

  async startTime(minutes: number) {
    this.show = false;
    await this.timeService.setTime(minutes);
  }

  async setCommentMode() {
    this.commentMode = !this.commentMode;
    await this.cardService.setCommitMode(this.commentMode);
  }

  async deleteAll() {
    this.clearAll.emit();
    await this.userService.resetAll();
    await this.router.navigate(['/']);
  }
}
