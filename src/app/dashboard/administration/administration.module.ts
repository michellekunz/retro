import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdministrationComponent } from './administration.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TimerModule } from '../timer/timer.module';

@NgModule({
  declarations: [AdministrationComponent],
  exports: [AdministrationComponent],
  imports: [CommonModule, FontAwesomeModule, TimerModule],
})
export class AdministrationModule {}
