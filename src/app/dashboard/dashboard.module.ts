import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { AdministrationModule } from './administration/administration.module';
import { BoardModule } from './board/board.module';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { TimerModule } from './timer/timer.module';
import { VoteModule } from './vote/vote.module';

@NgModule({
  declarations: [DashboardComponent],
  imports: [
    CommonModule,
    AdministrationModule,
    BoardModule,
    DashboardRoutingModule,
    TimerModule,
    VoteModule,
  ],
})
export class DashboardModule {}
