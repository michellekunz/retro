import { Component, NgZone, OnDestroy, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { environment } from '../../environments/environment';
import {
  AddCardEvent,
  CardEvent,
  DeleteCardEvent,
  DownVoteEvent,
  RearrangeCardsEvent,
  UpVoteEvent,
} from '../../../server/src/app.service';
import { Card, Type } from '../../../server/src/Card';
import { CardService } from '../services/card.service';
import { UserVotes } from '../../../server/src/UserVotes';
import { BnNgIdleService } from 'bn-ng-idle';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit, OnDestroy {
  commentMode = true;
  asAdmin: boolean;
  allVotes: UserVotes[] = [];
  timerDate: Date | null = null;
  positiveCards: Card[] = [];
  negativeCards: Card[] = [];
  userVotes = 0;

  constructor(
    public readonly userService: UserService,
    private readonly cardService: CardService,
    private readonly idleService: BnNgIdleService,
    private readonly route: Router,
    private readonly ngZone: NgZone,
  ) {
    this.asAdmin = this.userService.user?.isAdmin ?? false;
  }

  ngOnDestroy(): void {
    this.idleService.stopTimer();
  }

  ngOnInit(): void {
    this.initIdleService();
    this.initComponentData();

    const eventSourceCard = new EventSource(environment.apiUrl + '/events');
    eventSourceCard.onmessage = (messageEvent: MessageEvent) => {
      this.ngZone.run(() => {
        const cardEvent = JSON.parse(messageEvent.data) as CardEvent;
        if (cardEvent.eventType === 'timerUpdate') {
          this.timerDate = cardEvent.date ? new Date(cardEvent.date) : null;
        } else if (cardEvent.eventType === 'commentMode') {
          this.commentMode = cardEvent.enabled;
        } else if (cardEvent.eventType === 'add' || cardEvent.eventType === 'update') {
          this.addOrUpdate(cardEvent);
        } else if (cardEvent.eventType === 'delete') {
          this.deleteCard(cardEvent);
        } else if (cardEvent.eventType === 'upVote') {
          this.updateVotes(cardEvent, 1);
        } else if (cardEvent.eventType === 'downVote') {
          this.updateVotes(cardEvent, -1);
        } else if (cardEvent.eventType === 'rearrange') {
          this.rearrangeCards(cardEvent);
        } else if (cardEvent.eventType === 'reset') {
          this.resetAll();
        }
      });
    };
  }

  private initIdleService(): void {
    this.idleService.startWatching(300).subscribe((idleTimeOut: boolean) => {
      if (idleTimeOut) {
        if (!confirm('Hey! Bist du noch dabei?')) {
          this.userService.leave().then(() => this.route.navigateByUrl('/login'));
        }
      }
    });
  }

  private initComponentData(): void {
    this.cardService.getTime().then((date: Date) => (this.timerDate = date ? new Date(date) : null));
    this.cardService.getCommentMode().then((mode: boolean) => (this.commentMode = mode ?? false));
    this.cardService.getCards().then((cards: Card[]) => (cards ?? []).forEach(card => this.addOrUpdate(card)));
    this.userService.getVotes().then((votes: number) => (this.userVotes = votes ?? 0));
    this.userService.getAllVotes().then((votes: UserVotes[]) => (this.allVotes = votes ?? []));
  }

  private addOrUpdate(card: Card | AddCardEvent) {
    if (card.type == Type.NEGATIVE) {
      this.negativeCards = DashboardComponent.patchArray(this.negativeCards, {
        votes: card.votes,
        type: card.type,
        comment: card.comment,
        gifs: card.gifs,
        authors: card.authors,
        id: card.id,
      });
    } else {
      this.positiveCards = DashboardComponent.patchArray(this.positiveCards, {
        votes: card.votes,
        type: card.type,
        comment: card.comment,
        gifs: card.gifs,
        authors: card.authors,
        id: card.id,
      });
    }
  }

  private static patchArray(array: Card[], update: Card): Card[] {
    const existing = array.find((c: Card) => c.id === update.id);
    if (existing) {
      return array.map(card => card === existing ? update : card);
    } else {
      return [...array, update];
    }
  }

  private deleteCard(cardEvent: DeleteCardEvent) {
    const indexNegativeCard = this.negativeCards.findIndex((c: Card) => c.id === cardEvent.id);
    if (indexNegativeCard >= 0) {
      const cards = [...this.negativeCards];
      cards.splice(indexNegativeCard, 1);
      this.negativeCards = cards;
    }
    const indexPositiveCard = this.positiveCards.findIndex((c: Card) => c.id === cardEvent.id);
    if (indexPositiveCard >= 0) {
      const cards = [...this.positiveCards];
      cards.splice(indexPositiveCard, 1);
      this.positiveCards = cards;
    }
  }

  private rearrangeCards(cardEvent: RearrangeCardsEvent) {
    const orderOfIds = cardEvent.ids;
    this.negativeCards.sort(
      (a: Card, b: Card) => orderOfIds.indexOf(a.id) - orderOfIds.indexOf(b.id),
    );
  }

  private updateVotes(voteEvent: UpVoteEvent | DownVoteEvent, offset: number) {
    if (this.userService.user?.id === voteEvent.userId) {
      this.userVotes -= offset;
    }
    const card = this.negativeCards.find((c: Card) => c.id === voteEvent.cardId);
    if (card) {
      card.votes += offset;
    }
    this.userService.getAllVotes().then((votes: UserVotes[]) => (this.allVotes = votes ?? []));
  }

  resetAll(): void {
    this.negativeCards = [];
    this.positiveCards = [];
    this.timerDate = null;
    this.commentMode = true;
    this.asAdmin = false;
    this.allVotes = [];
    this.userVotes = 0;
  }
}
