import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Subscription, timer } from 'rxjs';
import { faStopwatch } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.scss'],
})
export class TimerComponent implements OnInit, OnChanges {
  stopwatch = faStopwatch;
  time = '';
  timeLeft = 0;
  subscription: Subscription | null = null;

  @Input()
  date!: Date | null;

  private static toTwoDigits(digit: number) {
    return String(digit).padStart(2, '0');
  }

  private static secondsToTime(seconds: number): string {
    const min = Math.floor(seconds / 60);
    const sec = seconds % 60;
    return (
      TimerComponent.toTwoDigits(min) + ':' + TimerComponent.toTwoDigits(sec)
    );
  }

  ngOnInit(): void {
    this.reset();
  }

  async calculateDuration(endDate: Date | null) {
    this.reset();

    if (endDate) {
      const milliseconds = endDate.getTime() - new Date().getTime();
      await this.startTime(Math.round(milliseconds / 1000));
    }
  }

  async startTime(timeLeft: number) {
    this.timeLeft = timeLeft;
    this.subscription = timer(100, 1000).subscribe(() => {
      this.timeLeft = this.timeLeft - 1;
      this.time = TimerComponent.secondsToTime(this.timeLeft);
      if (this.timeLeft < 0) {
        this.reset();
      }
    });
  }

  reset() {
    this.subscription?.unsubscribe();
    this.timeLeft = 0;
    this.time = TimerComponent.secondsToTime(0);
  }

  async ngOnChanges(changes: SimpleChanges) {
    if (changes.date) {
      await this.calculateDuration(this.date);
    }
  }
}
