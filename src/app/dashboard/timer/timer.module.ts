import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TimerComponent } from './timer.component';
import { FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [TimerComponent],
  exports: [TimerComponent],
  imports: [CommonModule, FormsModule, FontAwesomeModule],
})
export class TimerModule {}
